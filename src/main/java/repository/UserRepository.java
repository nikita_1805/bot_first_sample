package repository;

import com.mongodb.client.MongoCollection;
import org.bson.Document;
import repository.models.UserDB;

/**
 * @author andreikovalev on 3/18/19
 * @project bot_first_sample
 */
public class UserRepository implements Dao<UserDB>{
    private MongoCollection<Document> userCollection;

    public UserRepository(MongoCollection<Document> userCollection) {
        this.userCollection = userCollection;
    }

    public MongoCollection<Document> getUserCollection() {
        return userCollection;
    }


    @Override
    public void create(UserDB userDB) {

    }

    @Override
    public UserDB get(int id) {
        return null;
    }
}
