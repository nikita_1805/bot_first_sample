package repository.models;

/**
 * @author andreikovalev on 3/18/19
 * @project bot_first_sample
 */
public class UserDB {
    private int id;
    private String name;

    public UserDB(int id, String name) {
        this.id = id;
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }
}
