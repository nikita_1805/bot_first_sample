package repository;

/**
 * @author andreikovalev on 3/18/19
 * @project bot_first_sample
 */
public interface Dao<T> {
    void create(T t);

    T get(int id);
}
