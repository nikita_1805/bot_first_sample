
import modules.Module;
import org.telegram.telegrambots.api.methods.send.SendMessage;

public class SwitchChoice implements Module {
    private UserDB db;
    private SendMessage message;

    @Override
    public SwitchChoice checkFunction(String type) {

        if (type.equals("SwitchChoice")) {
            return new SwitchChoice();
        } else return null;
    }

    public SendMessage switchChoice(int counter, long chatId, long id) {

        switch (counter) {
            case 1:
                message = new SendMessage() // Create a message object object
                        .setChatId(chatId)
                        .setText("Nice shoot!");
                db.flagDelete(id);

                break;
            case 2:
                message = new SendMessage() // Create a message object object
                        .setChatId(chatId)
                        .setText("Here will be phone number");
                db.flagDelete(id);

                break;
            case 3:
                message = new SendMessage() // Create a message object object
                        .setChatId(chatId)
                        .setText("Here will be Address");
                db.flagDelete(id);

                break;

            case 4:
                message = new SendMessage() // Create a message object object
                        .setChatId(chatId)
                        .setText("Here will be E-mail");
                db.flagDelete(id);

                break;
            case 0:
                message = new SendMessage()
                        .setChatId(chatId)
                        .setText("Введите /start или выберите пункт меню" + "\n");

                break;

        }
        return message;
    }
}
