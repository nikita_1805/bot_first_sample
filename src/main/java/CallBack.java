import modules.Module;
import org.telegram.telegrambots.api.methods.send.SendMessage;
import org.telegram.telegrambots.api.objects.Update;

public class CallBack implements Module {
    private SendMessage new_message;
    private boolean check;

    @Override
    public CallBack checkFunction(String type){

        if (type.equals("Callback")){
            return new CallBack();
        }else

            return null;
    }

    public CallBack(){
        this.new_message = new SendMessage();
    }

    public SendMessage functionCall(Update update) {
        SendMessage new_message = new SendMessage();
        String call_data = update.getCallbackQuery().getData();
        long chat_id = update.getCallbackQuery().getMessage().getChatId();
        if (call_data.equals("FIO")) {

            new_message.setChatId(chat_id).setText("Введите ФИО");

        } else if (call_data.equals("PhoneNumber")) {

            new_message
                    .setChatId(chat_id)
                    .setText("Введите номер телефона");

        } else if (call_data.equals("HomeAddress")) {

            new_message
                    .setChatId(chat_id)
                    .setText("Введите адрес");
            return new_message;
        } else if (call_data.equals("E-mail")) {
            new_message
                    .setChatId(chat_id)
                    .setText("Введите E-mail");

        }

        return new_message;
    }
}
