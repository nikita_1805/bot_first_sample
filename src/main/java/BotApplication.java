import bot.BotConfig;
import bot.keyboards.CellKeyboard;
import com.mongodb.MongoClient;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import io.dropwizard.Application;
import io.dropwizard.setup.Bootstrap;
import io.dropwizard.setup.Environment;
import org.bson.Document;
import org.telegram.telegrambots.ApiContextInitializer;
import org.telegram.telegrambots.TelegramBotsApi;
import org.telegram.telegrambots.exceptions.TelegramApiException;


public class BotApplication extends Application<BotConfig> {
    public static void main(String[] args) throws Exception {
        new BotApplication().run(args);

    }
    @Override
    public void initialize(Bootstrap<BotConfig> bootstrap) {

    }

    @Override
    public void run(BotConfig configuration,
                    Environment environment) {
        final BotResource resource = new BotResource();
        environment.jersey().register(resource);

        ApiContextInitializer.init();

        MongoClient client = new MongoClient("localhost", 27017);
        MongoDatabase database = client.getDatabase("userdb_queries");
        MongoCollection<Document> userlist = database.getCollection("userlist");

        UserDB users = new UserDB(database);
        CellKeyboard kboard = new CellKeyboard();
        BotFunctions SetUp = new BotFunctions(users);
        TelegramBotsApi botsApi = new TelegramBotsApi();

        try {
            botsApi.registerBot(new Bot(users,SetUp, kboard));
        } catch (TelegramApiException e) {
            e.printStackTrace();
        }
    }
}



