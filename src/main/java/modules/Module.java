package modules;

public interface Module {
    <T> T checkFunction(String type);

}
